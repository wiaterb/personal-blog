# Personal blog

Running development
- `hugo server -D`

Adding post:
- `hugo new posts/my-first-post.md`

Fetch git submodules
- `git submodule update --init --recursive`

## Deployment
- `cd tf`
- `terraform init`
- `terraform apply -var bucket_name="flimsybucketname2" -auto-approve`
