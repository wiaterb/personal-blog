---
title: "Make Terraform Respect a S3 Bucket Created Manually"
date: 2021-08-17T21:41:46+02:00
summary: Make terraform and remote recourses state in sync
tags:
  - aws
  - terraform
---

When debuggin issues with `terraform` or a cloud provided it is sometimes easier to create bucket or other resources manully rather than in terraform script.
It can lead to a situation when your local terraform state is unsyncronized with buckets in the cloud when their names collide.

For example, you run command `terraform apply -auto-approve -var="bucket_name=personal-blog-master-v3"` which returns message:

```
aws_s3_bucket.staticpagebucket: Creating...
╷
│ Error: Error creating S3 bucket: BucketAlreadyOwnedByYou: Your previous request to create the named bucket succeeded and you already own it.
│ 	status code: 409, request id: 8VFF731HJBQHC63N, host id: poJi+2JhRzjT/KXh9PWG/yduZPSFHmv7zb6xSgShwEBxPHzvmZkoGYjccejXe6t4PWJ7jz+8IfA=
│
│   with aws_s3_bucket.staticpagebucket,
│   on bucket.tf line 9, in resource "aws_s3_bucket" "staticpagebucket":
│    9: resource "aws_s3_bucket" "staticpagebucket" {
│
╵
```

It means that the bucket with `personal-blog-master-v3` already exists. If this is the one that you want to keep during later work you can make `terraform` to accept this.
In order to do that use [`terraform import`](https://www.terraform.io/docs/cli/import/index.html).

In our example the command becomes `terraform import -input=false -var "bucket_name=personal-blog-master-v3" aws_s3_bucket.staticpagebucket personal-blog-master-v3` where

- `aws_s3_bucket.staticpagebucket` provider resource address
- `personal-blog-master-v3` resource id

If the command execution is successfull then following is returned

```
$ terraform import -input=false -var "bucket_name=personal-blog-master-v3" aws_s3_bucket.staticpagebucket personal-blog-master-v3
aws_s3_bucket.staticpagebucket: Importing from ID "personal-blog-master-v3"...
aws_s3_bucket.staticpagebucket: Import prepared!
  Prepared aws_s3_bucket for import
aws_s3_bucket.staticpagebucket: Refreshing state... [id=personal-blog-master-v3]
Import successful!
The resources that were imported are shown above. These resources are now in
your Terraform state and will henceforth be managed by Terraform.
```
