---
title: "Api Gateway enums"
date: 2020-08-03T22:44:06+01:00
tags:
    - api
    - gateway
---
Api gateway will not accept duplicated items in `enum`


```yaml
paths:
    /items:
    get:
        parameters:
        - in: query
            name: sort
            description: Sort order
            schema:
            type: string
            enum: [asc, desc, asc]
```
