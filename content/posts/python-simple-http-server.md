---
title: "Python Simple Http Server"
date: 2020-08-03T22:28:42+01:00
tags:
    - python
---

During development of huge static site you could use to see how behave build site (`public`).
- `cd public`
- `python -m http.server 8000`
- `http://localhost:8000/`

Source:
- https://docs.python.org/3/library/http.server.html
