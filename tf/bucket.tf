provider "aws" {
  region = "eu-west-2"
}

variable "bucket_name" {
  type = string
}

resource "aws_s3_bucket" "staticpagebucket" {
  bucket = var.bucket_name
  acl    = "public-read"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${var.bucket_name}/*"
        }
    ]
}
POLICY

  website {
    index_document = "index.html"
  }

  lifecycle_rule {
    enabled = true

    tags = {
      autoclean = true
      tf = true
    }

    expiration {
      days = 1
    }
  }
}

output "website_endpoint" {
  value = aws_s3_bucket.staticpagebucket.website_endpoint
}

output "website_domain" {
  value = aws_s3_bucket.staticpagebucket.website_domain
}
